﻿namespace bluetooth_connection
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Weggooien = new System.Windows.Forms.Button();
            this.Inhoud = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Weggooien
            // 
            this.Weggooien.Location = new System.Drawing.Point(145, 93);
            this.Weggooien.Name = "Weggooien";
            this.Weggooien.Size = new System.Drawing.Size(200, 81);
            this.Weggooien.TabIndex = 0;
            this.Weggooien.Text = "Weggooien";
            this.Weggooien.UseVisualStyleBackColor = true;
            this.Weggooien.Click += new System.EventHandler(this.button1_Click);
            // 
            // Inhoud
            // 
            this.Inhoud.Location = new System.Drawing.Point(145, 242);
            this.Inhoud.Name = "Inhoud";
            this.Inhoud.Size = new System.Drawing.Size(200, 73);
            this.Inhoud.TabIndex = 1;
            this.Inhoud.Text = "Inhoud";
            this.Inhoud.UseVisualStyleBackColor = true;
            this.Inhoud.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 460);
            this.Controls.Add(this.Inhoud);
            this.Controls.Add(this.Weggooien);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Weggooien;
        private System.Windows.Forms.Button Inhoud;
    }
}

